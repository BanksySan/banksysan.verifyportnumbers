[![Build Status](https://drone.io/bitbucket.org/BanksySan/banksysan.verifyportnumbers/status.png)](https://drone.io/bitbucket.org/BanksySan/banksysan.verifyportnumbers/latest)

# BanksySan.VerifyPortNumbers #

Single method, `verifyPortNumber(candidate)`.

When called with throw `Error` with a message in the form:

    Port must be a value an positive integer less than or equal to 65535.  You entered 'candidate'.

If the string doesn't represent a valid port number, that being an integer from 1 to 65535.