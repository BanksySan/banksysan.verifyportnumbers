/*global describe, it, before, beforeEach, after, afterEach */

describe('Getting the port number', function () {
    'use strict';
    var getPortNumber = require('../application/verify-port-number'),
        assert = require('chai').assert;

    describe('When the user passes a valid integer', function () {
        it('should return the integer (1)', function () {

            var candidate = '1';

            assert.doesNotThrow(function () {
                getPortNumber(candidate);
            }, candidate);
        });

        it('should return the integer (65536)', function () {
            var candidate = '65535';
            assert.doesNotThrow(function () {
                getPortNumber(candidate);
            }, candidate);
        });
    });

    describe('When an invalid por number is passed an exception is thrown', function () {
        it('empty string', function () {
            var candidate = '',
                expectedErrorMessage = getErrorMessage(candidate);

            assert.throw(function () {
                getPortNumber(candidate);
            }, Error, expectedErrorMessage);
        });

        it('negative integer', function () {
            var candidate = '-1',
                expectedErrorMessage = getErrorMessage(candidate);

            assert.throw(function () {
                getPortNumber(candidate);
            }, Error, expectedErrorMessage);
        });

        it('too higher number', function () {
            var candidate = '65536',
                expectedErrorMessage = getErrorMessage(candidate);

            assert.throw(function () {
                getPortNumber(candidate);
            }, Error, expectedErrorMessage);
        });

        it('alpha-numeric', function () {
            var candidate = '2a',
                expectedErrorMessage = getErrorMessage(candidate);

            assert.throw(function () {
                getPortNumber(candidate);
            }, Error, expectedErrorMessage);
        });

        it('is non integer floating point', function () {
            var candidate = '1.1',
                expectedErrorMessage = getErrorMessage(candidate);

            assert.throw(function () {
                getPortNumber(candidate);
            }, Error, expectedErrorMessage);
        });

        it('is integer floating point', function () {
            var candidate = '1.0',
                expectedErrorMessage = getErrorMessage(candidate);

            assert.throw(function () {
                getPortNumber(candidate);
            }, Error, expectedErrorMessage);
        });

    });

    function getErrorMessage(candidate) {
        return 'Port must be a value an positive integer less than or equal to 65535.  You entered \'' + candidate + '\'.';
    }
});
