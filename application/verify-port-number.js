module.exports = function verifyPortNumber(candidate) {
    "use strict";
    var integerRegex = /^\d+$/,
        parsed = parseInt(candidate);


    if (candidate.match(integerRegex) && parsed > 0 && parsed < 65536) {
        return;
    }

    throw new Error('Port must be a value an positive integer less than or equal to 65535.  You entered \'' + candidate + '\'.');
};
